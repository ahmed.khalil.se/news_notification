import pkg from '@temporalio/workflow';
const { defineWorkflow } = pkg;

// Define Temporal workflow
export const newsletterWorkflow = defineWorkflow(async () => {
  try {
    // Fetch news articles
    const articles = await fetchNewsArticles();

    // Generate email content
    const emailContent = generateEmailContent(articles);

    // Send email
    await sendEmail(emailContent);

    return 'Newsletter sent successfully';
  } catch (error) {
    console.error('Error sending newsletter:', error);
    throw new Error('Failed to send newsletter');
  }
});

// Helper functions
async function fetchNewsArticles() {
  try {
    const response = await axios.get('https://newsapi.org/v2/top-headlines', {
      params: {
        country: 'eg',
        apiKey: process.env.NEWS_API_KEY 
      }
    });

    return response.data.articles.slice(0, 20); // Return the top 20 news articles
  } catch (error) {
    console.error('Error fetching news articles:', error);
    throw new Error('Failed to fetch news articles');
  }
}

function generateEmailContent(articles) {
  let emailContent = '<h1>Top 20 News About Egypt</h1>';
  articles.forEach(article => {
    emailContent += `
      <div style="margin-bottom: 20px;">
        <h2>${article.title}</h2>
        <p>${article.description}</p>
        <p>Author: ${article.author || 'Unknown'}</p>
        <p>Date: ${new Date(article.publishedAt).toLocaleString()}</p>
        <img src="${article.urlToImage || ''}" alt="Article Image" style="max-width: 100%; height: auto;">
        <hr>
      </div>
    `;
  });

  return emailContent;
}

async function sendEmail(content) {
  try {
    let transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
      }
    });

    let mailOptions = {
      from: process.env.EMAIL_USER,
      to: process.env.SUBSCRIBER_EMAILS,
      subject: 'Daily Newsletter',
      html: content
    };

    await transporter.sendMail(mailOptions);
    console.log('Newsletter sent successfully');
  } catch (error) {
    console.error('Error sending newsletter:', error);
    throw new Error('Failed to send newsletter');
  }
}