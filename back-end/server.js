const express = require('express');
const path = require('path');
const { Worker } = require('@temporalio/worker');
const cron = require('node-cron');
const { WorkflowClient } = require('@temporalio/client');
const { newsletterWorkflow } = require('./temporalWorkflow.js');

const app = express();
const port = 8081; 

app.use(express.static(path.join(__dirname, 'build')));


app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// Start Temporal worker
async function startWorker() {
  const worker = new Worker({
    workflowsPath: new URL('./', import.meta.url),
    taskQueue: 'newsletter-task-queue'
  });

  worker.registerWorkflowImplementation('newsletterWorkflow', newsletterWorkflow);

  await worker.run();
}

startWorker().catch(err => {
  console.error('Failed to start worker:', err);
});

// Schedule Temporal workflow execution daily at 9 AM
cron.schedule('0 9 * * *', async () => {
  const client = new WorkflowClient();
  const { workflowId } = await client.startWorkflowExecution({
    workflowType: 'newsletterWorkflow',
    taskQueue: 'newsletter-task-queue'
  });

  console.log(`Newsletter workflow started with ID: ${workflowId}`);
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
