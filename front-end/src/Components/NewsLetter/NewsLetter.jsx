import PropTypes from 'prop-types';

const Newsletter = ({ article }) => {
  const { title, description, author, urlToImage, publishedAt } = article;

  // Format the published date
  const formattedDate = new Date(publishedAt).toLocaleDateString();

  return (
    <div className="newsletter">
      <h1>{title}</h1>
      <p>{description}</p>
      <p>Author: {author || 'Unknown'}</p>
      {urlToImage && <img src={urlToImage} alt="Article" />}
      <p>Date: {formattedDate}</p>
    </div>
  );
};

Newsletter.propTypes = {
  article: PropTypes.shape({
    title: PropTypes.string, 
    description: PropTypes.string.isRequired,
    author: PropTypes.string,
    urlToImage: PropTypes.string,
    publishedAt: PropTypes.string.isRequired,
  }).isRequired,
};

export default Newsletter;
