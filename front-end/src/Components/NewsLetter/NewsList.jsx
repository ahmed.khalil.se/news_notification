import './NewsList.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Newsletter from './NewsLetter';

const NewsList = () => {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    axios.get('https://newsapi.org/v2/top-headlines', {
      params: {
        country: 'eg',
        apiKey: 'cbb1dfe25e9e4260843f50bcd94f07d1',
      }
    }).then(response => {
      setArticles(response.data.articles);
    }).catch(error => {
      console.error('Error fetching news:', error);
    });
  }, []);

  return (
    <div className="news-list">
      <h1>Top News About Egypt</h1>
      {articles.map(article => (
        <Newsletter key={article.title} article={article} />
      ))}
    </div>
  );
};

export default NewsList;
