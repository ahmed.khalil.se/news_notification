import { useEffect, useState } from "react";
import Axios from "axios";
import { Newsletter, NewsletterForm, NewsList } from "./Components";
import { BrowserRouter, Routes, Route } from 'react-router-dom';

const App = () => {
    const [setData]=useState("");
  
    const getData = async () => {
      try {
        const response = await Axios.get("http://localhost:8081/getData");
        setData(response.data);
      } catch (error) {
        //;
      }
    }
  
    useEffect(() => {
      getData();
    }, );

    return (
        <BrowserRouter>
        <Routes>
          <Route path="/newsLetterForm" element={<NewsletterForm />} />
          <Route path="/NewsList" element={<NewsList />} />
          <Route path="/Newsletter" element={<Newsletter />} />
        </Routes>
        </BrowserRouter>
    )};
    export default App;